//***************//
//  Adva Gabay   //
//***************//

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h> 
#include "LineParser.h"

#define BUFFERSIZE 2048
#define PATH_MAX 4096


struct list
{
	int place;
	char command [BUFFERSIZE];
	struct list* next;
};
typedef struct list list;


void execute(cmdLine *pCmdLine);
void echo (char *cmdLine);
void cmd(cmdLine *c);
void insert(list** s, char data[PATH_MAX]);
void deleteFirst (list** s);
void printList(list** l);
void deleteList(list** l);
void historyPrint(list** l, char* place);
void command (char buffer [PATH_MAX],list** l);

int main ()
{
	char cwd [PATH_MAX];
	char buffer [BUFFERSIZE];
	list* l=NULL;
	while(1)
	{
		getcwd(cwd,PATH_MAX);
		printf("%s",cwd);
		printf(" $ ");
		fgets(buffer, BUFFERSIZE , stdin);
		command (buffer,&l);
	}
	deleteList(&l);
	return 0;
}

void command (char buffer [PATH_MAX],list** l)
{
	cmdLine* cmdl=NULL;
	insert (l, buffer);
	cmdl=parseCmdLines(buffer);
	if(strcmp(cmdl->arguments[0],"cd")==0)
		cmd(cmdl);
	else if(strcmp(cmdl->arguments[0],"myecho")==0)
		echo(buffer);
	else if(strcmp(cmdl->arguments[0],"history")==0)
		printList(l);
	else if(cmdl->arguments[0][0]=='!')
		historyPrint(l,cmdl->arguments[0]);
	else
		execute(cmdl);
	freeCmdLines(cmdl);
}


void cmd(cmdLine *c)
{
	pid_t proc =fork();
	int status;
	int s;
	int err;
	if(proc==-1)
	{
		perror (" fork error !:");
		exit (0);
	}
	if(proc==0)
	{
		exit(0);
	}
	err=chdir(c->arguments[1]);
	if(err!=0)
			perror("address error: ");
	if(!c->blocking)
	{
		waitpid(proc,&status,-1);
	}
	wait(&s);
}

void execute(cmdLine *pCmdLine)
{
	pid_t proc =fork();
	int status;
	int s;
	if(proc==-1)
	{
		perror (" fork error !:");
		exit (0);
	}
	if(proc==0)
	{
		execvp(pCmdLine->arguments[0],pCmdLine->arguments);
		perror("execute error\n");
		exit(0);
	}
	if(!pCmdLine->blocking)
	{
		waitpid(proc,&status,-1);
	}
	wait(&s); 
	printf("%d\n", WEXITSTATUS(s));
}


void echo (char *cmdLine)
{
	int i;
	for(i=7;cmdLine[i];i++)
		printf("%c",cmdLine[i]);
}

void insert(list** s, char data[PATH_MAX]) 
{
	list *temp, *r;
	temp = *s;
	int i=0;
	if (*s == NULL) 
	{
	    temp = ((list*)malloc(sizeof(list)));
	    strcpy(temp->command , data);
	    temp->place=i;
	    temp->next = NULL;
	    *s = temp;
	}
	else   
	{
	    while (temp->next != NULL)
	    {
	    	temp = temp->next;
	    	i+=1;
	    }
	    temp->place=i;
	    r = ((list*)malloc(sizeof(list)));
	    strcpy(r->command , data);
	    r->next = NULL;
	    r->place=i+1;
	    temp->next = r;
	    if(temp->place==14)
	    {
	    	deleteFirst (s);
	    }
	}
}

void deleteFirst (list** s)
{
	list* temp=*s;
	while(temp->next!=NULL)
	{
		temp->place-=1;
		temp= temp-> next;
	}
	temp=*s;
	*s=(*s)->next;
	free(temp);
}

void printList(list** l)
{
	list* temp=*l;
	if(*l==NULL)
	{
		printf("empty history\n");
	}
	while(temp)
	{
		{
			printf("%s",temp->command);
		}
		temp=temp->next;
	}
}

void deleteList(list** l)
{
	list* temp=*l;
	list* next;
	if(*l!=NULL)
	{
		while (temp != NULL)
	    {
	       next = temp->next;
	       free(temp);
	       temp = next;
	    }
	}
	*l=NULL;
}

void historyPrint(list** l, char* place)
{
	int placeP=atoi(place+1);
	int flag=1;
	list* temp=*l;
	if(placeP<0||placeP>15||*l==NULL)
	{
		printf("error- no matches history \n");
	}
	else
	{
		while(temp->next&&flag)
		{
			if(temp->place==placeP)
			{
				command (temp->command , l);
				flag=0;
			}
			temp=temp->next;
		}
		if(flag)
		{
			printf("error- no matches history \n");
		}

	}

}