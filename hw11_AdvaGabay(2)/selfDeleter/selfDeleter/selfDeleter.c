#include <Windows.h>
#include <stdio.h>


int main(int argc, char* argv[])
{
	DWORD check;
	LPCSTR path = (LPCSTR)malloc(1024);
	HANDLE file;
	BOOL var;
	DWORD num;
	HINSTANCE executeCheck;
	BYTE temp[1024] = { NULL };


	check = GetModuleFileNameA(NULL, path, 1024);
	if (check == 0)
	{
		printf("error receiveing the path :%u\n", GetLastError());
		system("PAUSE");
		return -1;
	}

	printf("%s\n", path);
	system("PAUSE");
	file = CreateFileA("deleter.bat", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);


	if (file == INVALID_HANDLE_VALUE)
	{
		printf("error opening file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}
	
	strcat(temp, ":label\ndel ");
	strcat(temp, path);
	strcat(temp, "\nIF EXIST ");
	strcat(temp, path);
	strcat(temp, "goto label\n");
	strcat(temp,"del deleter.bat\0");

	var = WriteFile(file, temp, strlen(temp), &num, NULL);
	if (var == FALSE)
	{
		printf("error writing to file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}

	CloseHandle(file);
	executeCheck = ShellExecuteA(NULL,NULL, "deleter", NULL, NULL, 5  );
	if (executeCheck < 32)
	{
		printf("execute error:%u\n", GetLastError());
	}

	free(path);
	system("PAUSE");
	return 0;
}