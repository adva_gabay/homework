//********************/
// Adva Gabay        //
//********************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main ()
{
	int fildes [2];
	int byte;
	int pipe1=pipe (fildes);
	pid_t f=fork();
	char buff [10];
	int status;
	if(pipe1==-1)
	{
		perror("pipe error:");
	}
	else
	{
		if(f==0)
		{
			close (fildes[0]);
			write(fildes[1], "magshimim", strlen("magshimim"));  
			exit(0);
		}
		wait(&status);
		close(fildes[1]);
		byte = read(fildes[0], buff, sizeof(buff));  
		printf("%s\n",buff);
	}
	return 0;
}
