//********************/
// Adva Gabay        //
//********************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h> 


int main ()
{
	int field [2];
	int pipe1=pipe(field);
	int f=fork();
	int status;
	char* args [3];
	char* args2 [4];
	args[0]="ls";
	args[1]="-l";
	args [2] =NULL;
	if(pipe1==-1)
	{
	        perror("pipe error:");
	}
	else
	{

		if(f==0)
		{
			close (1);
			dup(field[1]);
			close (field[1]);
			execvp(args[0],args);
			exit (0);
		}
		wait (&status);
		close (field [1]);
		args2[0]="tail";
		args2 [1]="-n";
		args2[2]="2";
		args2[3]=NULL;
		int fo= fork ();
		if(fo==0)
		{
			close (0);
			dup(field[0]);
			close (field [0]);
			execvp(args2[0],args2);
			exit (0);
		}
		close(field[0]);
		wait (&status);
	}
	return 0;
}	
