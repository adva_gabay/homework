#include <stdio.h>
#include <unistd.h>
#include <string.h>

void* malloc(size_t size);
void free (void* ptr);
void* realloc (void* ptr, size_t size);



typedef struct metadata_block * p_block;
struct metadata_block
{
	size_t size;
	p_block next;
	int free;
};


extern p_block heap;

