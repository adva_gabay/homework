#include "mm.h"

p_block heap=NULL; 


int main(int argc, char ** argv)
{
	char * str = malloc(10);
	void * ptr = (void *) str;
	void * ptr2;
	
	strcpy(str,"TEST!\0");

	free(str);
	printf("free: OK!\n");

	str = malloc(15);

	if ((void*) str != ptr)
	{
		printf("malloc() should've used the freed memory! old: %p new: %p\n",ptr,str);
		return 1;
	}

	strcpy(str,"test\n\0");
	ptr2 = realloc(str,10);

	malloc(10);
	ptr = realloc(str,20);

	if (ptr2 == ptr)
	{
		printf("realloc() should return a new memory block when expanding memory!\n");
		return 2;
	}
	
	printf("all good in the hood :)\n");
	return 0;
}


void* malloc(size_t size)
{
	p_block temp=heap;
	void* ptr;
	//if the first allocated memory
	if(temp==NULL)
	{
		heap=sbrk(sizeof(struct metadata_block));

		if(heap== (void*)-1)
		{
			return NULL;
		}
		temp=heap;
		ptr=sbrk(size);
		if(ptr== (void*)-1)
		{
			return NULL;
		}
		temp->free=0;
		temp->size=size;
		temp->next=NULL;
		return ptr;
	}


	while(temp->next!=NULL)
	{
		//check if there is "free" memoty in the linked list
		if(temp->free==1 && temp->size >=size)
		{
			temp->free=0;
			temp->size=size;
			return (temp+sizeof(struct metadata_block));
		}
		temp=temp->next;
	}

	//if there isn't, allocate new memory
	temp->next=sbrk(sizeof(struct metadata_block));
	if(temp->next== (void*)-1)
	{
		return NULL;
	}

	ptr=sbrk(size);
	if(ptr== (void*)-1)
	{
		return NULL;
	}
	temp=temp->next;
	temp->free=0;
	temp->size=size;
	temp->next=NULL;

	return ptr;
}

void free (void* ptr)
{
	//set pointer to the needed node in the list
	p_block toDelete=ptr-(sizeof(struct metadata_block));
	p_block temp=heap;
	int flag=1;

	if(toDelete==heap)
	{
		heap=heap->next;
		sbrk(-1*toDelete->size);
		sbrk(-1*(sizeof(struct metadata_block)));
	}

	else
	{
		if(toDelete->next!=NULL)
		{
			toDelete->free=1;
		}
		else
		{
			while(temp->next!=NULL&&flag==1)
			{
				if(temp->next->next==NULL)
				{
					sbrk(-1*toDelete->size);
					sbrk(-1*(sizeof(struct metadata_block)));
					temp->next=NULL;
					flag=0;
				}
				if(temp->next!=NULL)
				{
					temp=temp->next;
				}
			}
		}
	}
}

void* realloc (void* ptr, size_t size)
{
	//if ptr is NULL, the function act like malloc
	if(!ptr)
	{
		return (malloc(size));
	}

	//if the received size is smaller than the original size- returns the priginal pointer
	p_block temp=ptr-(sizeof(struct metadata_block));
	if(temp->size>=size)
	{
		return ptr;
	}
	//if needed to realloc:

	void* newP;
	newP=malloc(size);
	if(!size)
	{
		return NULL;
	}
	//copy the old memoty to the new allocated memory
	memcpy(newP, ptr, temp->size);
  	free(ptr);
 	return newP;
}







