#include <unistd.h>
#include <stdio.h>

void* malloc (size_t size);

void* malloc (size_t size)
{
	void* ptr=sbrk(size);
	if(ptr== (void*)-1)
	{
		return NULL;
	}
	return ptr;

}


int main ()
{
	int* a=malloc(sizeof(a));
	*a=1;
	printf("%d\n",*a);
	return 0;
} 
