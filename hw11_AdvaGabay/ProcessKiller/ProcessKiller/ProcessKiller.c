#include <Windows.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	DWORD pid;
	HANDLE proc;
	BOOL check;
	if (argc == 1)
	{
		printf("no input!\n");
		system("PAUSE");

		return -1;
	}
	pid = atoi(argv[1]);
	proc = OpenProcess(PROCESS_TERMINATE, FALSE, pid);
	if (proc == NULL)
	{
		printf("error opening handle:%u\n", GetLastError());
		system("PAUSE");

		return -1;
	}
	check = TerminateProcess(proc, 1337);
	if (check == 0)
	{
		printf("error terminating the process:%u\n", GetLastError());
		system("PAUSE");

		return -1;
	}
	check = CloseHandle(proc);
	if (check == 0)
	{
		printf("error closing the handle:%u\n", GetLastError());
		system("PAUSE");

		return -1;
	}
	printf(":)\n");
	system("PAUSE");
	return 0;
}