#include <Windows.h>
#include <stdio.h>


int main()
{
	DWORD check;
	LPCSTR path = (LPCSTR)malloc(1024);
	HANDLE file;
	BOOL var;
	DWORD num;
	HINSTANCE executeCheck;


	check = GetModuleFileNameA(NULL, path, 1024);
	if (check == 0)
	{
		printf("error receiveing the path :%u\n", GetLastError());
		system("PAUSE");
		return -1;
	}

	printf("%s\n", path);
	system("PAUSE");
	file = CreateFileA("deleter.bat", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);


	if (file == INVALID_HANDLE_VALUE)
	{
		printf("error opening file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}

	var = WriteFile(file, ":label\ndel ", 14, &num, NULL);

	if (var == FALSE)
	{
		printf("error writing to file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}

	var = WriteFile(file, path, check, &num, NULL);

	if (var == FALSE)
	{
		printf("error writing to file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}

	var = WriteFile(file, "\nIF EXIST ", 11, &num, NULL);

	if (var == FALSE)
	{
		printf("error writing to file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}

	var = WriteFile(file, path, check, &num, NULL);

	if (var == FALSE)
	{
		printf("error writing to file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}

	var = WriteFile(file, "\ngoto label\nELSE\ndel deleter.bat\0", 37, &num, NULL);

	if (var == FALSE)
	{
		printf("error writing to file:%u", GetLastError());
		system("PAUSE");
		return -1;
	}

	executeCheck = ShellExecuteA(NULL, "open", "deleter.bat", NULL, NULL, SW_SHOW);
	if (executeCheck > 32)
	{
		printf("execute error:%u\n", GetLastError());

	}

	CloseHandle(file);
	free(path);
	system("PAUSE");
	return 0;
}