#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

void quit();

int main ()
{
	sigset_t x;
	sigset_t y;
	sigset_t z;
	int i;
	int saved=dup(1);

	signal(SIGINT,&quit);
	signal(SIGQUIT,&quit);
	signal(SIGTERM,&quit);

	for(i=0;i<2000000000;i++)
	{
		if(i%1000000==0)
		{
			printf("I'm still alive, i=%d\n",i);
		}
	}

	exit(EXIT_SUCCESS);

	return 0;
}

void quit()
{
	char line [1024];
	printf("do you want to leave the program?\n");
	scanf("%s",line);
	int fd= open("output2.txt",O_WRONLY | O_CREAT | O_APPEND,S_IRWXU);
	strcat(line,"\n");
	write(fd,line,strlen(line));
	if(line[0]=='y'||line[0]=='Y')
	{
		kill(getpid(), 22);
	}
}