#include <elf.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>

//my readElf function
void myReadElf(char file [], int size);

int main (int argc, char* argv[])
{
	int file;
	struct stat status; //struct theat saves the size of the file
	
	//checks if received file
	if(argc>1)
	{
		file=open(argv[1],O_RDONLY);
		if(!file)
		{
			perror("error opening file");
			exit(1);
		}
		if(stat(argv[1],&status)!=0)//build the struct
		{
			perror("error opening file");
			exit(1);
		}

		myReadElf(argv[1],status.st_size);

	}
	exit (0);
}



void myReadElf(char file [], int size) 
{
  int fd = open(file, O_RDONLY);
  char* tab_p;
  int length;
  Elf32_Ehdr *ehdr;
  Elf32_Shdr *shdr;
  char *p ;
  int i;

  if(!fd)
  {
  	exit(0);
  }

  p = mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);

  ehdr= (Elf32_Ehdr*)p;//ehdr struct initialization
  shdr = (Elf32_Shdr *)(p + ehdr->e_shoff);//shdr struct initialization

  tab_p = p + shdr[ehdr->e_shstrndx].sh_offset;//pointer to the begining of the table(using to print the names- located at string table indexes)
  length = ehdr->e_shnum; 

//prints the table
  printf("%2s %15s %10s %5s %10s\n","No.","Name","Address","Offset","Size");
  for  (i = 0; i < length; ++i) 
  {
    printf("[%2d] ", i );
    printf("%15s",tab_p+shdr[i].sh_name);//uses the offset of the string table to find the name 
    printf("%10x", shdr[i].sh_addr);
    printf("%5x", shdr[i].sh_offset);
    printf("%10x\n", shdr[i].sh_size);
  }
}
