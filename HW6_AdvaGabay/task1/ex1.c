#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main ()
{
	int f=fork();
	close(1);
	open("output.txt",O_WRONLY | O_CREAT | O_APPEND,S_IRWXU);
	if(f==0)
	{
		while (1)
		{
			printf("I'm still alive!\n");
		}
	}
	sleep (1);
	printf("Dispatching\n");
	kill(f, SIGKILL);
	printf("Dispatched\n");
	return 0;
}